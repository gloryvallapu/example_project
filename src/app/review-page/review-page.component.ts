import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-review-page',
  templateUrl: './review-page.component.html',
  styleUrls: ['./review-page.component.css']
})
export class ReviewPageComponent implements OnInit {
  revData: any;
  data: { "id": any; "name": string; "rating": number; }[];
  ratingData: any;
  item: { "id": any; "name": string; "rating": number; };

  constructor() {
    this.data = [
      { "id": 1, "name": "lakki", "rating": 2 },
      { "id": 2, "name": "gayathri", "rating": 3 },
      { "id": 3, "name": "neeima", "rating": 4 },
      { "id": 4, "name": "radhika", "rating": 2 },
      { "id": 5, "name": "swapna", "rating": 2 },
    ];
  }

  ngOnInit() {
    this.review();
  }

  review() {
    this.revData = this.data;
    console.log(this.revData);
  }

  ratingStore(storeddata:any){
    this.item=this.data.find(((i:any)=>i.id===storeddata.itemId));
    if(this.item!=undefined){
      this.item.rating=storeddata.rating;
      this.ratingData=storeddata.rating;
    }
  }


}
