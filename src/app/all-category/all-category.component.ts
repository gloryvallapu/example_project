import { Component, OnInit } from '@angular/core';
import { MainServiceService } from '../main-service.service';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-all-category',
  templateUrl: './all-category.component.html',
  styleUrls: ['./all-category.component.css']
})
export class AllCategoryComponent implements OnInit {
  category: any;
  categoryname: any;
  cat: any;
  catname: any;
  catDataList: any =[];
  catList: any;

  constructor(private api: MainServiceService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.cat = this.route.params.subscribe(param => {
      this.catname = param['a'];
    })
    this.catData();
  }

  catData() {
    let url = "categories";
    //this.category = { "param_other1": this.catname }
    this.api.getWithOneParam(url, this.catname).subscribe(data => {
      console.log(data);
      if (data.status == "success") {
        this.catDataList = data.categories;
        this.catDataList.forEach(data => {
          this.catList = data.subcategoryname;
          console.log(this.catList.subcategoryname);
        });
        console.log(this.catData);
        // this.router.navigate(['/allCategory']);
      }
    });
  }

  gotoCategory(subcategoryname) {
    let c = this.catname;
    let b = subcategoryname;
    console.log(b);
    this.router.navigate(['/catPage',c, b]);
  }

}
