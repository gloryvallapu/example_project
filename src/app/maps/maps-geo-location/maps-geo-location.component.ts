import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-maps-geo-location',
  // templateUrl: './maps-geo-location.component.html',
  template: 
  `
  <div class="container" style="margin-top: 50px;">
  <img src="https://maps.googleapis.com/maps/api/staticmap?center={{city}}&zoom=7&size=200x200&key=AIzaSyBAyMH-A99yD5fHQPz7uzqk8glNJYGEqus" style="margin-top: 50px;"/>
  </div>
  `,
  styleUrls: ['./maps-geo-location.component.css']
})
export class MapsGeoLocationComponent implements OnInit {
  // cities: { "city": string; }[];
  cities: string =  'Rome';

  constructor() {
    // this.cities = [
    //   {"city":"hyderabad" },
    //   {"city":"machilipatnam" },
    //   {"city":"vijayawada" },
    //   {"city":"guntur" },
    //   {"city":"kothapudi" },
    //   {"city":"benguluru" },
    //   {"city":"pune" },
    //   {"city":"pedana" },
    // ];
  }

  ngOnInit() {
  }

  goto(nextCity) {
    this.cities = nextCity;
  }

}
