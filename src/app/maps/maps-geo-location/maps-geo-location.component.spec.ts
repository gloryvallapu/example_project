import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapsGeoLocationComponent } from './maps-geo-location.component';

describe('MapsGeoLocationComponent', () => {
  let component: MapsGeoLocationComponent;
  let fixture: ComponentFixture<MapsGeoLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapsGeoLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapsGeoLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
