import { Component, OnInit, ViewChild } from '@angular/core';
import { MapsGeoLocationComponent } from '../maps-geo-location/maps-geo-location.component';

@Component({
  selector: 'app-get-geo-location',
  // templateUrl: './get-geo-location.component.html',
  template: `
  <div class="container" style="margin-top: 50px;">
    <input type="text" #inputText placeholder="Search a city" />
    <button (click)="showCities()">Go</button>
    <hr />
    <app-maps-geo-location #map></app-maps-geo-location>
  </div>
  `,
  styleUrls: ['./get-geo-location.component.css']
})
export class GetGeoLocationComponent implements OnInit {
  @ViewChild('inputText') loc: any;
  @ViewChild('map') map: MapsGeoLocationComponent;

  constructor() { }

  ngOnInit() {
  }

  showCities(): void {
    this.map.goto(this.loc.nativeElement.value);  
    this.loc.nativeElement.focus();          // assign focus
    this.loc.nativeElement.value = '';       // clean input
  }
  
}
