import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetGeoLocationComponent } from './get-geo-location.component';

describe('GetGeoLocationComponent', () => {
  let component: GetGeoLocationComponent;
  let fixture: ComponentFixture<GetGeoLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetGeoLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetGeoLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
