import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from "@angular/http";
import 'rxjs/add/operator/map';
// import { Observable } from 'rxjs/Rx';
//import 'rxjs/Rx'

@Injectable({
  providedIn: 'root'
})
export class MainServiceService {

  apiUrl = 'http://192.168.20.65:5000/'
  // apiUrl='http://sivalakshmi.pythonanywhere.com/'

  /* powertex server url */
  apiUrl2 = 'http://157.119.108.142:8002/get/';
  posturl = 'http://157.119.108.142:8002/';

  /* powertex local url */
  // apiUrl = 'http://192.168.20.65:8000/get/';
  // posturl = 'http://192.168.20.65:8000/';

  options: any;
  response: any;

  constructor(public http: Http) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('secret_key', '1234');
    this.options = new RequestOptions({ headers: headers });
  };



  //PUT Method (PUT)
  updateData(url, body) {
    return this.http.put(this.apiUrl + url, body, this.options).map(res => res.json());
  };

  //DELETE Method (DELETE)
  deleteData(url) {
    return this.http.delete(this.apiUrl + url, this.options).map(res => res.json());
  };

  // GET Method 
  getData(url) {
    return this.http.get(this.apiUrl + url, this.options).map(res => res.json());
  };

  // GET Method with data
  getallData(url, obj) {
    return this.http.get(this.apiUrl + url + obj, this.options).map(res => res.json());
  };

  // GET Method with 1 param 
  getWithOneParam(url, obj) {
    console.log(obj)
    return this.http.get(this.apiUrl + url + '?param_other1=' + obj, this.options).map(res => res.json());
  };

  //GET Method with 2 params
  getWithTwoParams(url, obj) {
    console.log(url, obj);
    return this.http.get(this.apiUrl + url + '?param_other1=' + obj.param_other1 + '&param_other2=' + obj.param_other2, this.options).map(res => res.json());
  };

  // Get Method with arguments
  getDataWithArg(url, obj) {
    return this.http.get(this.apiUrl + url + '?username=' + obj.username + '&password=' + obj.password, this.options).map(res => res.json());
  };

  // Get Method with username and otp arguments
  getDataWithuserOtpArg(url, obj) {
    return this.http.get(this.apiUrl + url + '?username=' + obj.username + '&otp=' + obj.otp, this.options).map(res => res.json());
  };

  // POST Method
  postData(url, data) {
    return this.http.post(this.apiUrl + url, data, this.options).map(res => res.json());
  };

   // post Method with arguments
   postDataWithoneArg(url, obj) {
    return this.http.post(this.apiUrl + url, obj, this.options).map(res => res.json());
  };


  /* powertex services list */

  // // POST Method powertex_service
  postDatawithPosturl(url, data) {
    return this.http.post(this.posturl + url, data, this.options).map(res => res.json());
  };

  // GET Method powertex_service
  getDatawithInput_id(input_id) {
    return this.http.get(this.apiUrl2 + '?input_id=' + input_id, this.options).map(res => res.json());
  };


}