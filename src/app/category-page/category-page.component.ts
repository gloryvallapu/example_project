import { Component, OnInit } from '@angular/core';
import { MainServiceService } from '../main-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { CommunicationService } from '../communication.service';

@Component({
  selector: 'app-category-page',
  templateUrl: './category-page.component.html',
  styleUrls: ['./category-page.component.css']
})
export class CategoryPageComponent implements OnInit {
  subcat: any;
  subcatname: any;
  catgoryname: any;
  subcategory: { "param_other1": any; "param_other2": any; };
  allData: any;
  prodData: { "image": any; "subcategory": any; "name": any; "modelno": any; "upload_mrp": any; "upload_discount": any; "upload_netPrice": any; };
  catgry: any;
  subcatgry: any;
  name: any;
  modelno: any;
  mrp: any;
  disc: any;
  netPrice: any;
  image: any;
  loginUserData: any;
  user_name: any;
  token: any;
  productDetail: any;
  obj: any = {};
  wishData: { 'user_id': any; "modelNo": any; };


  constructor(private api: MainServiceService, private router: Router, private route: ActivatedRoute, private ngxSmartService: NgxSmartModalService, private eventemit: CommunicationService) { }

  ngOnInit() {
    this.token = localStorage.getItem('token');
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    this.subcat = this.route.params.subscribe(param => {
      this.subcatname = param['b'];
      this.catgoryname = param['c'];
    })
    this.subcatData();
  }

  subcatData() {
    let url = "catsubcat";
    this.subcategory = { "param_other1": this.catgoryname, "param_other2": this.subcatname }
    this.api.getWithTwoParams(url, this.subcategory).subscribe(data => {
      if (data.status == "success") {
        this.allData = data.categories.subcategoryname;
        console.log(data);
      }
    });
  }

  gotoProductDetail(data) {
    let url = "productdtl";
    console.log("data", data);
    //  this.user_name = this.loginUserData.email;
    this.image = data.image;
    this.subcatgry = data.subcategory;
    this.name = data.name;
    this.modelno = data.modelno;
    this.mrp = data.upload_mrp;
    this.disc = data.upload_discount;
    this.netPrice = data.upload_netPrice;

    let obj = data;
    localStorage.setItem('data', JSON.stringify(obj));

    this.prodData = { "image": this.image, "subcategory": this.subcatgry, "name": this.name, "modelno": this.modelno, "upload_mrp": this.mrp, "upload_discount": this.disc, "upload_netPrice": this.netPrice }

    this.api.postDataWithoneArg(url, this.prodData).subscribe(data => {
      console.log(data);
      this.productDetail = data.data;
      localStorage.setItem('proddata', JSON.stringify(this.productDetail));
      this.router.navigate(['/productDetail']);
    });
  }

  wish_list(obj, val) {
    if (this.token != "" && this.token != undefined) {
      this.addWishlist(obj, val);
    }
    else {
      this.ngxSmartService.getModal('loginModal').open();
    }
  }

  // addWishlist(obj, val) {
  //   let url = "whislist";
  //   this.modelno = obj.modelno;
  //   this.wishData = { 'user_id': this.token, "modelNo": this.modelno }
  //   this.api.postDataWithoneArg(url, this.wishData).subscribe(data => {
  //     console.log(data);
  //   });
  // }

  addWishlist(obj, val) {
    // let body = { "s.no": obj.seq_no, "productid": obj.productid, "user_id": this.loginUserData.user_id, "wishlist": val }
    let body = {"productid": obj.modelno, "user_id": this.loginUserData.user_id, "wishlist": val }
    console.log(body)
    let url = "wishlist_insert/";
    this.api.postDatawithPosturl(url, body).subscribe(data => {
      console.log(data);
      if (data.Status == 1) {
        this.obj.wishList_count = data.count;
        // this.eventemit.fire(this.obj);
      } else if (data.Status == 0) {
        this.obj.wishList_count = data.count;
        // this.eventemit.fire(this.obj);
      }
    });
  }

}
