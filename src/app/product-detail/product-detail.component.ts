import { Component, OnInit } from '@angular/core';
import { MainServiceService } from '../main-service.service';
import { Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { CommunicationService } from '../communication.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  loginUserData: any;
  prodData: any;
  productDetail: any;
  body: { "user_id": any; "modelNo": any; "qty": string; };
  username: any;
  modelno: any;
  token: any;
  obj: any = {};
  getData: any;
  cartData: any;

  constructor(private api: MainServiceService, private router: Router, private ngxSmartModalService: NgxSmartModalService, private eventemit: CommunicationService) {
    this.obj.id = 1;
  }

  ngOnInit() {
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    this.token = localStorage.getItem('token');
    // console.log("prod", JSON.parse(localStorage.getItem('proddata')));
    this.productDetail = JSON.parse(localStorage.getItem('proddata'));
  }

  addToCart() {
    if (this.token != "" && this.token != undefined) {
      let url = "addtocart";
      this.body = { "user_id": this.token, "modelNo": this.productDetail.modelno, "qty": "1" };
      console.log(this.body);
      this.api.postData(url, this.body).subscribe(data => {
        if (data.status == "item added to cart") {
          console.log(data.status);
          // this.obj.cartItem_count = data.count;
          // this.eventemit.fire(this.obj);
          this.addToCartGet();
        }
      });
    }
    else {
      this.ngxSmartModalService.getModal('loginModal').open();
    }
  }

  addToCartGet() {
    let url = "addtocart";
    console.log(this.prodData);
    this.api.getWithOneParam(url, this.token).subscribe(data => {
      if (data.status == "success") {
        this.cartData = data.data;
        this.obj.cartItem_count = data.count;
        this.eventemit.fire(this.obj);
        console.log(this.cartData);
      }
      // this.grandtotal = 0;
      // this.cartData.forEach(data => {
      //   this.grandtotal = this.grandtotal + ((data.sub_tot));
      // });
    });
  }

}
