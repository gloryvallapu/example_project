import { BrowserModule } from '@angular/platform-browser';
import { NgModule , CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSmartModalModule, NgxSmartModalService } from 'ngx-smart-modal';
import { RouterModule, Routes } from "@angular/router";
import { HttpModule } from "@angular/http";
import { MaterialModule } from './material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { CarouselModule } from 'angular4-carousel';

import { MainServiceService } from "./main-service.service";
import { Broadcaster } from './app.broadcaster';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LoginModalComponent } from './login-modal/login-modal.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AllCategoryComponent } from './all-category/all-category.component';
import { CategoryPageComponent } from './category-page/category-page.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ViewCartComponent } from './view-cart/view-cart.component';
import { ReviewPageComponent } from './review-page/review-page.component';
import { RatingPageComponent } from './rating-page/rating-page.component';
import { FileUploadsComponent } from './file-uploads/file-uploads.component';
import { MapsGeoLocationComponent } from './maps/maps-geo-location/maps-geo-location.component';
import { GetGeoLocationComponent } from './maps/get-geo-location/get-geo-location.component';
import { WishlistComponent } from './wishlist/wishlist.component';

const approot: Routes = [
  { path: "home", component: HomeComponent },
  { path: "login-modal", component: LoginModalComponent },
  { path: "dashboard", component: DashboardComponent },
  { path: "allCategory/:a", component: AllCategoryComponent },
  { path: "catPage/:c/:b", component: CategoryPageComponent },
  { path: "productDetail", component: ProductDetailComponent },
  { path: "viewCart", component: ViewCartComponent },
  { path: "reviewPage", component: ReviewPageComponent },
  { path: "mapPage", component: MapsGeoLocationComponent },
  { path: "fileUploadPage", component: FileUploadsComponent },
  { path: "wishlistPage", component: WishlistComponent },
  { path: "", redirectTo: "/home", pathMatch: "full", data: { breadcrumbs: 'home' } }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    LoginModalComponent,
    DashboardComponent,
    AllCategoryComponent,
    CategoryPageComponent,
    ProductDetailComponent,
    ViewCartComponent,
    ReviewPageComponent,
    RatingPageComponent,
    FileUploadsComponent,
    MapsGeoLocationComponent,
    GetGeoLocationComponent,
    WishlistComponent
  ],
  imports: [
    BrowserModule,
    NgxSmartModalModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    MaterialModule,
    BrowserAnimationsModule,
    // CarouselModule,
    RouterModule.forRoot(approot)
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [NgxSmartModalService, MainServiceService, Broadcaster],
  bootstrap: [AppComponent]
})
export class AppModule { }
