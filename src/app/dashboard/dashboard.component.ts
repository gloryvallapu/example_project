import { Component, OnInit } from '@angular/core';
import { MainServiceService } from '../main-service.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  token: any;
  loginUserData: string;
  body: { "token": any; };
  category: any;
  categoryname: any;
  catData: any;


  constructor(private api: MainServiceService, private router: Router) { }

  ngOnInit() {
    this.token = localStorage.getItem('token');
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    // this.categories();
  }

  // gotoLogout() {
  //   let url = "logout";
  //   this.body = { "token": this.token }
  //   this.api.postDataWithoneArg(url, this.body).subscribe(data => {
  //     console.log(data);
  //     this.token = "";
  //     this.loginUserData = "";
  //     // localStorage.clear();
  //     this.router.navigate(['/home']);
  //   })
  // }

  // categories() {
  //   let url = "allcategories";
  //   this.api.getData(url).subscribe(data => {
  //     console.log(data);
  //     if (data.status == "success") {
  //       this.category = data.categories;
  //     }
  //   });
  // }

  // gotoAllCat(categoryname) {
  //   let a = categoryname;
  //   console.log(a);
  //   this.router.navigate(['/allCategory',a]);
  // }

}
// this.router.navigate(['/allCategory']);