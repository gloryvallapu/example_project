import { Component, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import { MainServiceService } from '../main-service.service';

@Component({
  selector: 'app-file-uploads',
  templateUrl: './file-uploads.component.html',
  styleUrls: ['./file-uploads.component.css']
})
export class FileUploadsComponent implements OnInit {
  jsondata: {}[];
  json_data: string;
  uploadfile: any;
  url: string;
  attrurl: string;
  flag: number;
  body: { "flag": number; "data": string; };
  uploadAttrfile: any;
  prod_data: any;
  EXCEL_TYPE: any;
  EXCEL_EXTENSION: string;

  constructor(private api: MainServiceService) { }

  ngOnInit() {
  }

  //For Excel-Upload code
  productexcelUpload(evt: any): void {
    this.uploadfile = evt.target.files[0].name;
    console.log(this.uploadfile);
    this.onFileChange(evt.target, 'prod');
  };

  onFileChange(evt: any, data) {
    const file: File = evt.files[0];
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {

      if (data == "prod") {
        this.url = "https://img.icons8.com/color/50/000000/ms-excel.png";
      } else if (data == "attr") {
        this.attrurl = "https://img.icons8.com/color/50/000000/ms-excel.png";
      }
      /* read workbook */
      const result: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(result, { type: 'binary', cellDates: true, cellText: false, dateNF: 'yyyy/mm/dd' });

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      // this.data = <AOA>(XLSX.utils.sheet_to_json(ws, {header: 1}));
      this.jsondata = (XLSX.utils.sheet_to_json(ws));
      this.json_data = JSON.stringify(this.jsondata);
      console.log(this.json_data);
    };
    reader.readAsBinaryString(evt.files[0]);
  }

  uploadData() {
    console.log(this.json_data);
    let url = "product_master_data/";
    this.flag = 1;
    this.body = { "flag": 1, "data": this.json_data };
    this.api.postDatawithPosturl(url, this.body).subscribe(data => {
      if (data.status == "success") {
        alert(data.status);
        console.log(data.status);
      }
    });
  }

  //For Attributes-Upload code
  attrexcelUpload(evt: any): void {
    this.uploadAttrfile = evt.target.files[0].name;
    this.onFileChange(evt.target, 'attr');
  }

  uploadAttributesData() {
    console.log(this.json_data);
    let url = "product_master_data/";
    this.flag = 2;
    this.body = { "flag": 2, "data": this.json_data };
    this.api.postDatawithPosturl(url, this.body).subscribe((data) => {
      console.log(data);
      if (data.status == "success") {
        alert('success');
      }
    });
  }

  //For Download code
  excelproddownload() {
    this.api.getDatawithInput_id('4.3').subscribe((data) => {
      console.log(data);
      this.prod_data = data;
      this.downloadJsontoExcel();
    });
  }

  //For Download Json_to_Excel
  downloadJsontoExcel() {
    var excelFileName = "Sample";
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.prod_data);
    console.log('worksheet', worksheet);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: this.EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + this.EXCEL_EXTENSION);
  };

}
