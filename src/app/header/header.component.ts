import { Component, OnInit } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Router } from '@angular/router';
import { MainServiceService } from '../main-service.service';
import { CommunicationService } from '../communication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  body: { "token": any; };
  token: any;
  loginUserData: any;
  cartItem_count: any;

  constructor(public ngxSmartModalService: NgxSmartModalService, private router: Router, private api: MainServiceService, private eventOn: CommunicationService) { }

  ngOnInit() {
    this.token = localStorage.getItem('token');
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));

    this.eventOn.on().subscribe(data => {
      if (data.id == 1) { //cart Updation
        this.cartItem_count = data.cartItem_count;
      }
      // else if (data.id == 2) { //logout
      //   this.token = "";
      //   this.loginUserData = "";
      //   localStorage.clear();
      // } else if (data.id == 3) { //Profile Updation
      //   this.loginUserData = data.loginUserData;
      // } else if (data.id == 4) { //WishList Updation
      //   this.wishList_count = data.wishList_count;
      //   console.log(this.wishList_count);
      // }

    });

  }

  gotoreview(){
    this.router.navigate(['/reviewPage']);
  }
  gotoMaps(){
    this.router.navigate(['/mapPage']);
  }
  gotoFileUpload(){
    this.router.navigate(['/fileUploadPage']);
  }
  

  getLoginModal() {
    this.ngxSmartModalService.getModal('loginModal').open();
  }

  gotoLogout() {
    let url = "logout";
    this.body = { "token": this.token }
    this.api.postDataWithoneArg(url, this.body).subscribe(data => {
      console.log(data);
      this.token = "";
      this.loginUserData = "";
      localStorage.clear();
      this.router.navigate(['/home']);
    });
  }

  gotoWishlist(){
    this.router.navigate(['/wishlistPage']);
  }

  gotoviewCart() {
    this.router.navigate(['/viewCart']);
  }

}
