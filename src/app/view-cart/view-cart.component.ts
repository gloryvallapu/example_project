import { Component, OnInit } from '@angular/core';
import { MainServiceService } from '../main-service.service';
import { Router } from '@angular/router';
import { CommunicationService } from '../communication.service';

@Component({
  selector: 'app-view-cart',
  templateUrl: './view-cart.component.html',
  styleUrls: ['./view-cart.component.css']
})
export class ViewCartComponent implements OnInit {
  prodData: { "user_id": any; };
  cartData: any;
  token: any;
  updateCart: any;
  upCart: { "user_id": any; "qty": any; };
  removeItem: any;
  delCart: any;
  grandtotal: any;
  obj: any = {};

  constructor(private api: MainServiceService, private router: Router, private eventemit: CommunicationService) { 
    this.obj.id = 1;
  }

  ngOnInit() {
    this.token = localStorage.getItem('token');
    this.viewcart();
  }

  viewcart() {
    let url = "addtocart";
    console.log(this.prodData);
    this.api.getWithOneParam(url, this.token).subscribe(data => {
      if (data.status == "success") {
        this.cartData = data.data;
        this.obj.cartItem_count = data.count;
        this.eventemit.fire(this.obj);
        console.log(this.cartData);
      }
      this.grandtotal = 0;
      this.cartData.forEach(data => {
        this.grandtotal = this.grandtotal + ((data.sub_tot));
      });
    });
  }

  increment(data) {
    data.qty++;
    this.updatecartprod(data);
  }

  decrement(data) {
    if (data.qty > 1) {
      data.qty--;
      this.updatecartprod(data);
    }
  }

  // Update 
  updatecartprod(data) {
    let url = "addtocart";
    this.upCart = { "user_id": this.token, "qty": data.qty };
    console.log(this.upCart);
    this.api.updateData(url, this.upCart).subscribe(data => {
      if (data.status == "updated") {
        console.log(data);
        this.viewcart();
      }

    });
  }

  // Delete 
  deleteIem(modelno) {
    let url = "addtocart";
    this.removeItem = modelno;
    this.delCart = 'addtocart?user_id=' + this.token + '&modelNo=' + this.removeItem;
    // this.delCart = { "user_id": this.token, "modelNo": this.removeItem };
    this.api.deleteData(this.delCart).subscribe(data => {
      if (data.status == "Deleted") {
        console.log(data);
        // this.obj.cartItem_count = data.count;
        // this.eventemit.fire(this.obj);
        this.viewcart();
      }
    });
  }


}
