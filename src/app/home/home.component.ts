import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MainServiceService } from '../main-service.service';
// import { ICarouselConfig, AnimationConfig } from 'angular4-carousel';
// import { CarouselService } from 'angular4-carousel';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  token: any;
  loginUserData: any;
  category: any;


  constructor(private api: MainServiceService, private router: Router) {
    // private x: CarouselService
  }

  ngOnInit() {
    this.token = localStorage.getItem('token');
    this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
    this.categories();
  }

  // for caurosel start here 
  // public imageSources: string[] = [
  //   '../assets/one.jpg',
  //   '../assets/two.jpg',
  //   '../assets/three.jpg'
  // ];

  // public config: ICarouselConfig = {
  //   verifyBeforeLoad: true,
  //   log: false,
  //   animation: true,
  //   animationType: AnimationConfig.SLIDE,
  //   autoplay: true,
  //   autoplayDelay: 5000,
  //   stopAutoplayMinWidth: 768
  // };
  // for caurosel end here 

  categories() {
    let url = "allcategories";
    this.api.getData(url).subscribe(data => {
      console.log(data);
      if (data.status == "success") {
        this.category = data.categories;
      }
    });
  }

  gotoAllCat(categoryname) {
    let a = categoryname;
    console.log(a);
    this.router.navigate(['/allCategory',a]);
  }

}
