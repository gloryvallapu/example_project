import { Component, OnInit, ViewChild } from '@angular/core';
import { NgxSmartModalService } from "ngx-smart-modal";
import { MainServiceService } from '../main-service.service';
import { Router } from "@angular/router";
import { HeaderComponent } from '../header/header.component';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.css']
})
export class LoginModalComponent implements OnInit {

  //showLogin: any = true;
  regDetails: any = {};
  showRegister: boolean;
  showLogin: boolean;
  showFrgtPwd: boolean;
  body: { "username": any; "password": any; };
  token: any;
  loginUserData: any;
  userId: boolean;
  OTP: boolean;
  otpSubmit: boolean;
  sendotp: boolean;
  forgotUser: { "username": any; };
  passwordBlock: boolean;
  otpBody: { "username": any; "otp": any; };
  otpData: any = {};
  pwdBody: { "username": any; "new_password": any; "confirm_password": any; };
  @ViewChild(HeaderComponent) HeaderComponent;

  constructor(private ngxSmartModalService: NgxSmartModalService, private api: MainServiceService, private router: Router, private headerComponent: HeaderComponent) { }

  loginData: any = {};

  ngOnInit() {
    this.showLogin = true;
  }

  gotoRegForm() {
    this.showLogin = false;
    this.showRegister = true;
    this.showFrgtPwd = false;
    this.passwordBlock = false;
  }

  regFormSubmit() {
    console.log("post", this.regDetails);
    let url = "test123";
    // debugger;
    this.api.postData(url, this.regDetails).subscribe(data => {
      console.log(data);
      if (data['status'] == "success") {
        this.regDetails = {};
        alert("Form Registration Successfully Added");
        this.showRegister = false;
        this.showLogin = true;
        this.showFrgtPwd = false;
        this.passwordBlock = false;

      }
    })
  }

  // loginSubmit() {
  //   let testMethod = "test123";
  //   console.log(this.loginData);
  //   this.body = { "username": this.loginData.email, "password": this.loginData.password };
  //   this.api.getDataWithArg(testMethod,this.body).subscribe(data => {
  //   //or
  //   //this.api.getDataWithArg(testMethod, this.loginData).subscribe(data => {
  //     if (data['status'] == "success") {
  //       alert('Success');
  //       this.router.navigate(['/dashboard']);
  //       this.ngxSmartModalService.close('loginModal');
  //     }
  //     else {
  //       alert("Incorrect Logins");
  //     }
  //   })
  // }

  loginSubmit() {
    let url = "test123";
    this.body = { "username": this.loginData.user, "password": this.loginData.password };
    this.api.getDataWithArg(url, this.body).subscribe(data => {

      if (data.status == "success") {
        this.token = data.id;
        localStorage.setItem('token', this.token);
        this.loginUserData = data.regd[0];
        localStorage.setItem('loginUserData', JSON.stringify(this.loginUserData));
        alert('Success');
        this.headerComponent.token=this.token;
        this.headerComponent.loginUserData=this.loginUserData;
        this.router.navigate(['/dashboard']);
        this.ngxSmartModalService.close('loginModal');
      }
      else {
        alert("Incorrect email and password");
      }
    });
  };

  forgotPwd() {
    this.showRegister = false;
    this.showLogin = false;
    this.passwordBlock = false;
    this.showFrgtPwd = true;
    this.userId = true;
    this.OTP = false;
    this.otpSubmit = false;
    this.sendotp = true;
  }

  sendOtp() {
    let url = "forgot";
    this.forgotUser = { "username": this.loginData.user };
    this.api.postDataWithoneArg(url, this.forgotUser).subscribe(data => {
      if (data.status == "success") {
        this.otpData = data;
        this.sendotp = false;
        this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
        console.log(data);
      }
    });
  }

  verifyOTP() {
    let url = "forgot";
    this.otpBody = { "username": this.loginData.user, "otp": this.loginData.otp };
    this.api.getDataWithuserOtpArg(url, this.otpBody).subscribe(data => {
      if (data.status == "success") {
        // console.log(data);
        this.passwordBlock = true;
        this.showFrgtPwd = false;
      }
    });
  }

  resetPwd() {
    let url = "reset";
    this.pwdBody = { "username": this.loginData.user, "new_password": this.loginData.new_password, "confirm_password": this.loginData.confirm_password };
    this.api.postData(url, this.pwdBody).subscribe(data => {
      if (data.status == "password changed successfully") {
        // this.loginUserData = JSON.parse(localStorage.getItem('loginUserData'));
        // console.log(data);
        // alert(data.status);
        this.router.navigate(['/dashboard']);
        this.ngxSmartModalService.close('loginModal');
        this.passwordBlock = false;
        // this.showFrgtPwd = true;
      }
      else {
        alert(data.status);
      }
    });
  }


}
